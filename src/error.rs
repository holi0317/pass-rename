error_chain! {
    foreign_links {
        Io(::std::io::Error);
        Var(::std::env::VarError);
        Walk(::walkdir::Error);
        StripPrefix(::std::path::StripPrefixError);
    }

    errors {
        NoPassDir(path: String) {
            description("Pass directory does not exist"),
            display("Pass directory '{}' is not a directory. Run `pass init` for creating a new pass database", path)
        }

        EntryNotUnicode {
            description("A pass entry is not encoded in unicode")
        }

        ErrExitCode {
            description("Pass commmand exited with non-zero exit code")
        }
    }
}
