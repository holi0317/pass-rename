extern crate dirs;
extern crate walkdir;
#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate log;

mod error;
mod pass;

use error::{ErrorKind, Result};
use std::collections::{HashMap, HashSet};
use std::path::PathBuf;
use walkdir::{DirEntry, WalkDir};

fn is_not_hidden(entry: &DirEntry) -> bool {
    entry.depth() == 0
        || !entry
            .path()
            .ancestors()
            .take(entry.depth())
            .filter_map(|path| path.file_name())
            .filter_map(|name| name.to_str())
            .any(|s| s.starts_with("."))
}

pub fn run() -> Result<()> {
    let root = pass::get_pass_dir()?;

    // Not using WalkDir::max_depth so that we could use filter_entry method
    let max_depth = WalkDir::new(&root)
        .into_iter()
        .filter_entry(is_not_hidden)
        .map(|e| e.map(|e| e.depth()).unwrap_or(0))
        .max()
        .unwrap_or(0);

    for depth in 1..max_depth {
        let plan = make_plan(&root, depth)?;
        execute_plan(&plan)?;
    }

    Ok(())
}

fn make_plan(root: &PathBuf, depth: usize) -> Result<HashMap<String, String>> {
    let mut plan = HashMap::new();

    for entry in WalkDir::new(&root)
        .min_depth(depth)
        .max_depth(depth)
        .into_iter()
        .filter_entry(is_not_hidden)
    {
        let entry = entry?;
        let path = entry.path();
        let extension = path.extension();
        let path = path.strip_prefix(&root)?;

        let path = match path.to_str() {
            Some(s) => s,
            None => bail!(ErrorKind::EntryNotUnicode),
        };

        let extension_len = match extension {
            Some(ext) => ext.len(),
            None => 0,
        };

        let mut path = String::from(path);
        let path_len = path.len();

        // Remove extension from path
        if extension_len > 0 {
            path.truncate(path_len - extension_len - 1);
        }

        let new_path = path.replace(" ", "-").to_lowercase();
        if path != new_path {
            plan.insert(path.to_string(), new_path);
        }
    }

    Ok(plan)
}

fn execute_plan(plan: &HashMap<String, String>) -> Result<()> {
    let mut seen = HashSet::new();
    let mut ignore_val = HashSet::new();

    for value in plan.values() {
        if !seen.contains(value) {
            seen.insert(value);
        } else {
            ignore_val.insert(value);
            warn!("Found duplicate result paths: '{}'. Ignoring them", value);
        }
    }

    for (path, new_path) in plan.iter().filter(|(_, value)| !ignore_val.contains(value)) {
        pass::pass_mv(path, new_path)?;
    }

    debug!("Transition end");

    Ok(())
}
