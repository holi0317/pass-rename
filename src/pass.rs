use std::env::{self, VarError};
use std::path::{Path, PathBuf};
use std::process::Command;

use dirs::home_dir;
use error::{ErrorKind, Result};

const DIR_ENV: &str = "PASSWORD_STORE_DIR";

/// Get pass directory
///
/// If pass directory is not available, Error would be return.
pub fn get_pass_dir() -> Result<PathBuf> {
    let dir = match env::var(DIR_ENV) {
        Ok(val) => Path::new(&val).to_path_buf(),
        Err(VarError::NotPresent) => get_default_pass_dir(),
        Err(VarError::NotUnicode(s)) => Err(VarError::NotUnicode(s))?,
    };

    if !dir.is_dir() {
        let path = dir.to_str().expect("Home dir should be valid UTF-8 string");
        bail!(ErrorKind::NoPassDir(String::from(path)));
    }

    Ok(dir)
}

/// Get default pass directory
/// Should be `~/.password-store`
fn get_default_pass_dir() -> PathBuf {
    let mut home_dir = home_dir().expect("Cannot get home directory");
    home_dir.push(".password-store");
    home_dir
}

/// Issue mv command in pass
pub fn pass_mv(from: &str, to: &str) -> Result<()> {
    debug!("Spawning command `pass mv {} {}`", from, to);

    let child = Command::new("pass").arg("mv").arg(from).arg(to).output()?;

    debug!("Command exited with code {}", child.status);
    debug!("Command stdout: {}", String::from_utf8_lossy(&child.stdout));
    debug!("Command stderr: {}", String::from_utf8_lossy(&child.stderr));

    if !child.status.success() || child.stderr.len() > 0 {
        bail!(ErrorKind::ErrExitCode);
    }

    Ok(())
}
