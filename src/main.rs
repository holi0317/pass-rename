extern crate env_logger;
extern crate pass_rename;

fn main() {
    env_logger::init();

    if let Err(error) = pass_rename::run() {
        println!("{}", error);
    }
}
