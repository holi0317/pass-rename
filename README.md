# pass-rename
Rename [pass](https://www.passwordstore.org/) entries.

## Rename rules
This would enforce the following rules

 - All space are replaced by dash
 - All capital case is lowered

If there is any conflict in pass entries, those entries would not be touched
but others will still be processed.

## Requirements
 - [Rust](https://rustup.rs/) (Tested on nightly 1.30, but stable 1.29 should be fine)
 - [pass](https://www.passwordstore.org/) exists in `$PATH`

## Usage
```bash
cargo run

# For verbose output, use the following command
RUST_LOG=debug cargo run
```
